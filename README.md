# README #

The project explores a conflict predictor system: given a conflict situation description,
and a person description, making a prediction of what conflict strategy is the person most
likely to choose. Probabilistic Soft Logic (PSL) is used to create the predictor.

For more information please consult the wiki
accessible through the sidebar or the link below:

https://bitbucket.org/pfontain/psl-conflict-strategy-classification/wiki/Home