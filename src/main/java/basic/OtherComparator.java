/*
 * This file is part of the PSL software.
 * Copyright 2011-2013 University of Maryland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package basic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import edu.umd.cs.psl.database.Database;
import edu.umd.cs.psl.evaluation.statistics.filter.AtomFilter;
import edu.umd.cs.psl.model.argument.GroundTerm;
import edu.umd.cs.psl.model.atom.GroundAtom;
import edu.umd.cs.psl.model.atom.ObservedAtom;
import edu.umd.cs.psl.model.predicate.Predicate;
import edu.umd.cs.psl.util.database.Queries;
import edu.umd.cs.psl.model.argument.StringAttribute;

import edu.umd.cs.psl.evaluation.statistics.*;

public class OtherComparator implements PredictionComparator {

	private static final String DEFAULT_RESULTS_FILE_PATH =  "../conflict-data-cleaning/data-psl/results.csv";
	public static final double DEFAULT_THRESHOLD = 0.5;

	private final Database result;
	private Database baseline;
	private AtomFilter resultFilter;
	private double threshold;

	int tp;
	int fn;
	int tn;
	int fp;

	Map<GroundAtom, Double> errors;
	Set<GroundAtom> correctAtoms;


	public OtherComparator(Database result) {
		this.result = result;
		baseline = null;
		resultFilter = AtomFilter.NoFilter;
		threshold = DEFAULT_THRESHOLD;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	@Override
	public void setBaseline(Database db) {
		this.baseline = db;
	}

	@Override
	public void setResultFilter(AtomFilter af) {
		resultFilter = af;
	}

	/**
	 * Compares the baseline with te inferred result for a given predicate
	 * DOES NOT check the baseline database for atoms. Only use this if all 
	 * possible predicted atoms are active and unfiltered
	 */
	@Override
	public DiscretePredictionStatistics compare(Predicate p) {
		countResultDBStats(p);
		//System.out.println("tp " + tp + " fp " + fp + " tn " + tn + " fn " + fn);
		return new DiscretePredictionStatistics(tp, fp, tn, fn, threshold, errors, correctAtoms);
	}

	/**
	 * Compares the baseline with the inferred result for a given predicate. 
	 * Checks the baseline database for atoms
	 * 
	 * @param Predicate p : The predicate to compare
	 * @param int maxAtoms : Defines the maximum number of base atoms that can be found for the given predicate. (This will vary, depending on the predicate and the problem.)
	 */
	@Override
	public DiscretePredictionStatistics compare(Predicate p, int maxBaseAtoms) {
		countResultDBStats(p);

		Iterator<GroundAtom> res = resultFilter.filter(Queries.getAllAtoms(baseline, p).iterator());
		double expected;
		while (res.hasNext()) {
			GroundAtom baselineAtom = res.next();

			if (!errors.containsKey(baselineAtom) && !correctAtoms.contains(baselineAtom)) {
				//Missed result
				expected = (baselineAtom.getValue() >= threshold) ? 1.0 : 0.0;

				if (expected != 0.0) {
					errors.put(result.getAtom(baselineAtom.getPredicate(), baselineAtom.getArguments()), expected);
					fn++;
				}
			}
		}

		tn = maxBaseAtoms - tp - fp - fn;
		return new DiscretePredictionStatistics(tp, fp, tn, fn, threshold, errors, correctAtoms);
	}

	/**
	 * Subroutine used by both compare methods for counting statistics from atoms
	 * stored in result database
	 * @param p Predicate to compare against baseline database
	 */
	private void countResultDBStats(Predicate p) {
		tp = 0;
		fn = 0;
		tn = 0;
		fp = 0;


		errors = new HashMap<GroundAtom,Double>();
		correctAtoms = new HashSet<GroundAtom>();

		GroundAtom resultAtom, baselineAtom;
		GroundTerm[] args;
		boolean actual, expected;

		Iterator<GroundAtom> iter = resultFilter.filter(Queries.getAllAtoms(result, p).iterator());

        try {
              BufferedWriter resultFileWriter = new BufferedWriter(new FileWriter(new File(DEFAULT_RESULTS_FILE_PATH)));


		while (iter.hasNext()) {
			resultAtom = iter.next();
			args = new GroundTerm[resultAtom.getArity()];
			for (int i = 0; i < args.length; i++)
				args[i] = (GroundTerm) resultAtom.getArguments()[i];
			baselineAtom = baseline.getAtom(resultAtom.getPredicate(), args);
			
			String labelPrediction = ((StringAttribute)resultAtom.getArguments()[1]).getValue();
			if(labelPrediction.equals("Active")){
				resultFileWriter.write(resultAtom.getValue() + "," + (int)baselineAtom.getValue());
                resultFileWriter.newLine();
			}

			actual = (resultAtom.getValue() >= threshold);
			expected = (baselineAtom.getValue() >= threshold);
			if (actual) {	
				if (expected) {
					// True negative
					if (baselineAtom.getArguments()[1].toString().equals("\'Passive\'"))
						tn++;
					// True positive
					else
						tp++;
					correctAtoms.add(resultAtom);
				}
				// False negative
				else if (baselineAtom.getArguments()[1].toString().equals("\'Passive\'")) {
					fn++;
					errors.put(resultAtom, -1.0);
				}
				// False positive
				else {
					fp++;
					errors.put(resultAtom, 1.0);
				}
			}
		}

			resultFileWriter.close();

				System.out.println("tp " + tp + " tn " + tn + " fp " + fp + " fn " + fn);
		float truePositiveRate = tp / (float)(tp + fn);
		float trueNegativeRate = tn / (float)(tn + fp);
		float accuracy = (tp + tn) / (float)(fn + fp + tp + tn); 
		System.out.println("accuracy " + accuracy + " truePositiveRate " + truePositiveRate + " trueNegativeRate " + trueNegativeRate);

		} catch (FileNotFoundException e) {
			System.err.println("Unable to find "+DEFAULT_RESULTS_FILE_PATH+". Stopping measure calculation.");
        } catch (IOException e){
        	System.err.println("Error writing to "+DEFAULT_RESULTS_FILE_PATH+". Stopping measure calculation.");
        }

		
	}

}