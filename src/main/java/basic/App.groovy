package basic;

import edu.umd.cs.psl.config.*;
import edu.umd.cs.psl.groovy.*;
import edu.umd.cs.psl.database.DataStore;
import edu.umd.cs.psl.database.Database;
import edu.umd.cs.psl.database.Partition;
import edu.umd.cs.psl.application.inference.LazyMPEInference;
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.LazyMaxLikelihoodMPE;
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxLikelihoodMPE;
import edu.umd.cs.psl.application.learning.weight.maxlikelihood.MaxPseudoLikelihood;
import edu.umd.cs.psl.database.ReadOnlyDatabase;
import edu.umd.cs.psl.model.atom.GroundAtom;
import edu.umd.cs.psl.model.argument.ArgumentType;
import edu.umd.cs.psl.database.rdbms.RDBMSDataStore;
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver;
import edu.umd.cs.psl.database.rdbms.driver.H2DatabaseDriver.Type;
import edu.umd.cs.psl.model.function.ExternalFunction;
import edu.umd.cs.psl.model.argument.GroundTerm;
import edu.umd.cs.psl.util.database.Queries;
import edu.umd.cs.psl.ui.loading.InserterUtils;
import edu.umd.cs.psl.database.loading.Inserter;
import edu.umd.cs.psl.model.argument.StringAttribute;
import edu.umd.cs.psl.evaluation.statistics.DiscretePredictionComparator;
import edu.umd.cs.psl.evaluation.statistics.DiscretePredictionStatistics;

def usePrelearnedWeights = false
def prelearnedWeights = [1.0019209083653606,2.3746196973531735,3.9956342289082714,1.1095099722856105,2.4379931172091163,3.892520852677953,1.1176615495651925]
def defaultWeights = [1.0,1.0,1.0,1.0,1.0,1.0,1.0]
def useDevSet = false
String dataFolder = ["..","conflict-data-cleaning","data-psl"].join(java.io.File.separator)

////////////////////////// initial setup ////////////////////////
ConfigManager cm = ConfigManager.getManager()
ConfigBundle config = cm.getBundle("basic")

def defaultPath = System.getProperty("java.io.tmpdir")
String dbpath = config.getString("dbpath", defaultPath + File.separator + "app")
DataStore data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, dbpath, true), config)
PSLModel m = new PSLModel(this, data)

// Entity ids
m.add predicate: "worker" , types: [ArgumentType.UniqueID]
m.add predicate: "response" , types: [ArgumentType.UniqueID]
m.add predicate: "scenario"  , types: [ArgumentType.UniqueID]

// Extraversion for each worker
// TODO: Add required Worker (Ask psl expert)
// ?Add minimum and maximum value?
m.add predicate: "extraversion" , types: [ArgumentType.UniqueID, ArgumentType.Double]

// Neuroticism for each worker
m.add predicate: "neuroticism" , types: [ArgumentType.UniqueID, ArgumentType.Double]

// Physical activity
m.add predicate: "physical" , types: [ArgumentType.UniqueID, ArgumentType.Integer]

// Physical activity
m.add predicate: "age" , types: [ArgumentType.UniqueID, ArgumentType.Integer]

// Relationship for each scenario
// TODO: Add required Scenario
// ?Add minimum and maximum value?
m.add predicate: "relationship" , types: [ArgumentType.UniqueID, ArgumentType.Integer]

// Worker who made response
// TODO: Add required Response and Worker
m.add predicate: "responded" , types: [ArgumentType.UniqueID,ArgumentType.UniqueID]

// Scenario of the response
// TODO: Add required Response and Scenario
m.add predicate: "responseScenario", types: [ArgumentType.UniqueID,ArgumentType.UniqueID]

// Active label for the response
// TODO: Add required Response
// TODO: Add constraint for active(r,"Active") + active(r,"Passive") < 1
m.add predicate: "active" , types: [ArgumentType.UniqueID, ArgumentType.String]

m.add function: "isHigh", implementation: new BFIisHigh()
m.add function: "isLow", implementation: new BFIisLow()
m.add function: "isHighP", implementation: new PhysicalIsHigh()
m.add function: "isLowP", implementation: new PhysicalIsLow()
m.add function: "olderAdult", implementation: new OlderAdult()
m.add function: "youngerAdult", implementation: new YoungerAdult()
m.add function: "sameExtraversion" , implementation: new ExactSimilarity()

GroundTerm activeStringAttribute = new StringAttribute("Active")
GroundTerm passiveStringAttribute = new StringAttribute("Passive")

def weights = defaultWeights

if(usePrelearnedWeights)
	weights = prelearnedWeights
else
	weights = defaultWeights

// If a worker responds one way in a scenario it is likely the response will be similar in another scenario
m.add rule : ( responded(A,X) & responded(B,X) & active(A,C)) >> active(B,C),  weight : weights[0]

// Extroverts tend to have active responses and introverts have passive responses
m.add rule : ( responded(A,B) & extraversion(B,C) & isHigh(C)) >> active(A,activeStringAttribute),  weight : weights[1]
m.add rule : ( responded(A,B) & extraversion(B,C) & isLow(C)) >> active(A,passiveStringAttribute),  weight : weights[2]

// If two responders have similar extraversion values they will tend to have similar responses to the same scenario
m.add rule : ( responseScenario(A,D) & responseScenario(B,D) & responded(A,X) & responded(B,Y) & extraversion(X,F) & extraversion(Y,G) & sameExtraversion(F,G) & active(A,C)) >> active(B,C),  weight : weights[3]

// Responders with higher neuroticism values tend to have less active responses
m.add rule : ( responded(A,B) & neuroticism(B,C) & isLow(C)) >> active(A,activeStringAttribute),  weight : weights[4]
m.add rule : ( responded(A,B) & neuroticism(B,C) & isHigh(C)) >> active(A,passiveStringAttribute),  weight : weights[5]

// Those who do more physical exercise have more active responses
m.add rule : ( responded(A,B) & physical(B,C) & isHighP(C)) >> active(A,activeStringAttribute),  weight : weights[1]
m.add rule : ( responded(A,B) & physical(B,C) & isLowP(C)) >> active(A,passiveStringAttribute),  weight : weights[2]

// Younger adults tend to have active responses
m.add rule : ( responded(A,B) & age(B,C) & youngerAdult(C)) >> active(A,activeStringAttribute),  weight : weights[1]
m.add rule : ( responded(A,B) & age(B,C) & olderAdult(C)) >> active(A,passiveStringAttribute),  weight : weights[2]

// Responses to the same scenario tend to be similar
m.add rule : ( responseScenario(A,X) & responseScenario(B,X) & active(A,C)) >> active(B,C),  weight : weights[6]

// NOTE: Takes too much time
// m.add rule : ( worker(X) & worker(Y) & response(A) & response(B) & extraversion(X,C) & extraversion(Y,D) & sameExtraversion(C,D) & active(A,activeStringAttribute)) >> active(B,activeStringAttribute),  weight : 5

m.add PredicateConstraint.PartialFunctional , on : active

println m

// Adding data

def partition = new Partition(0)

def predicates = [extraversion,neuroticism,physical,age,responded,responseScenario]
def filePredicates = ["extraversion","neuroticism","physical","age","responded","response_scenario"]


Inserter insert
String file_name
String file_path
for ( int iPredicate = 0; iPredicate < predicates.size(); iPredicate++) {
    insert = data.getInserter(predicates[iPredicate], partition)
    file_name = filePredicates[iPredicate] + "_" + "train" + ".txt"
	file_path = [dataFolder,file_name].join(java.io.File.separator)
	InserterUtils.loadDelimitedData(insert,file_path);
}

Database db = data.getDatabase(partition, [Responded, ResponseScenario, Extraversion, Neuroticism, Physical, Age] as Set);

if(!usePrelearnedWeights){
	// Weight Learning
	Partition trueDataPartition = new Partition(1);
	insert = data.getInserter(active, trueDataPartition)
	InserterUtils.loadDelimitedDataTruth(insert, [dataFolder,"active" + "_" + "train" + ".txt"].join(java.io.File.separator));

	println "Learning weights ..."
	Database trueDataDB = data.getDatabase(trueDataPartition, [Active] as Set);
	LazyMaxLikelihoodMPE weightLearning = new LazyMaxLikelihoodMPE(m, db, trueDataDB, config);
	weightLearning.learn();
	weightLearning.close();

	println "Learned model:"
	println m

	trueDataDB.close()
}

// Testing
def setName = useDevSet ? "dev" : "test"

println "Testing "+setName+" ..."
Partition sn2 = new Partition(2);
for ( int iPredicate = 0; iPredicate < predicates.size(); iPredicate++) {
    insert = data.getInserter(predicates[iPredicate], sn2)
    file_name = filePredicates[iPredicate] + "_" + setName + ".txt"
	file_path = [dataFolder,file_name].join(java.io.File.separator)
	InserterUtils.loadDelimitedData(insert,file_path);
}

println "Inferring ..."
Database db2 = data.getDatabase(sn2, [Responded, ResponseScenario, Extraversion, Neuroticism, Physical, Age] as Set);
inferenceApp = new LazyMPEInference(m, db2, config);
result = inferenceApp.mpeInference();
inferenceApp.close();

/*
println "Inference results on test set:"
for (GroundAtom atom : Queries.getAllAtoms(db2, Active))
	println atom.toString() + "\t" + atom.getValue();
*/

println "Comparing ..."
Partition testTrueDataPartition = new Partition(3);
insert = data.getInserter(active, testTrueDataPartition)
InserterUtils.loadDelimitedDataTruth(insert, [dataFolder,"active" + "_" + setName + ".txt"].join(java.io.File.separator));
Database testTrueDataDB = data.getDatabase(testTrueDataPartition, [Active] as Set);
/*
for (GroundAtom atom : Queries.getAllAtoms(testTrueDataDB, Active))
	println atom.toString() + "\t" + atom.getValue();
*/
def comparator = new OtherComparator(db2)
comparator.setBaseline(testTrueDataDB)
comparator.setThreshold(0.5)
def statistics = comparator.compare(Active)
//println "threshold " + statistics.getThreshold() + " accuracy " + statistics.getAccuracy() + 
//		" precision positive " + statistics.getPrecision(DiscretePredictionStatistics.BinaryClass.POSITIVE) +
//		" precision negative " + statistics.getPrecision(DiscretePredictionStatistics.BinaryClass.NEGATIVE)


testTrueDataDB.close()
/* We close the Databases to flush writes */
db.close()
db2.close()

class BFIisHigh implements ExternalFunction {
	
	@Override
	public int getArity() {
		return 1;
	}

	@Override
	public ArgumentType[] getArgumentTypes() {
		return [ArgumentType.Double].toArray();
	}
	
	@Override
	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
		return args[0].getValue() > 3.5 ? 1.0 : 0.0;
	}
	
}

class BFIisLow implements ExternalFunction {
	
	@Override
	public int getArity() {
		return 1;
	}

	@Override
	public ArgumentType[] getArgumentTypes() {
		return [ArgumentType.Double].toArray();
	}
	
	@Override
	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
		return args[0].getValue() < 2.5 ? 1.0 : 0.0;
	}
	
}

class PhysicalIsHigh implements ExternalFunction {
	
	@Override
	public int getArity() {
		return 1;
	}

	@Override
	public ArgumentType[] getArgumentTypes() {
		return [ArgumentType.Integer].toArray();
	}
	
	@Override
	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
		return args[0].getValue() > 3.5 ? 1.0 : 0.0;
	}
	
}

class PhysicalIsLow implements ExternalFunction {
	
	@Override
	public int getArity() {
		return 1;
	}

	@Override
	public ArgumentType[] getArgumentTypes() {
		return [ArgumentType.Integer].toArray();
	}
	
	@Override
	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
		return args[0].getValue() < 2.5 ? 1.0 : 0.0;
	}
	
}

class OlderAdult implements ExternalFunction {
	
	@Override
	public int getArity() {
		return 1;
	}

	@Override
	public ArgumentType[] getArgumentTypes() {
		return [ArgumentType.Integer].toArray();
	}
	
	@Override
	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
		return args[0].getValue() >= 5 ? 1.0 : 0.0;
	}
	
}

class YoungerAdult implements ExternalFunction {
	
	@Override
	public int getArity() {
		return 1;
	}

	@Override
	public ArgumentType[] getArgumentTypes() {
		return [ArgumentType.Integer].toArray();
	}
	
	@Override
	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
		return args[0].getValue() <= 3 ? 1.0 : 0.0;
	}
	
}

class ExactSimilarity implements ExternalFunction {
	
	@Override
	public int getArity() {
		return 2;
	}

	@Override
	public ArgumentType[] getArgumentTypes() {
		return [ArgumentType.Double, ArgumentType.Double].toArray();
	}
	
	@Override
	public double getValue(ReadOnlyDatabase db, GroundTerm... args) {
		return (args[0].getValue() - args[1].getValue()) < 0.5 ? 1.0 : 0.0;
	}
	
}