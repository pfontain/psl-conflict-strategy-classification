\documentclass[11pt,letterpaper,oneside]{article}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[left=1in,right=1in,top=1in,bottom=1in]{geometry}
\usepackage{cite}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{subcaption}
% \linespread{1.08}

\newtheorem{predicate}{Definition}

\begin{document}

\title{Conflict Strategy Prediction}
\author{Paulo F. Gomes}
\date{\textit{\today}}

\maketitle

\section{Introduction}

In this report I explored how to develop a conflict strategy prediction system: given a conflict situation description, and a person description, making a prediction of what conflict strategy is the person most likely to choose. The study used crowd-source collected data gathered in \cite{swanson12}. The corpus has responses to hypothetical conflict situations, person descriptions (demographic and personality self-assessment), and labels for the the type of the conflict strategy used. We will call the participants answering what they would do in a scenario as responders.

In summary, the construction of the original corpus consisted of the following steps:
\begin{enumerate}
\item collect narratives: crowd sourced workers were prompted online for a short textual description of an experienced conflict scenario including  its outcome.
\item hypothetical scenario creation: outcome and personal details were removed from the scenarios. The authors changed the perspective from first person to second (e.g. \textit{My friend wrecked my car ...} to \textit{Your friend wrecked your car ...}).
\item collect responses: crowd sourced workers were prompted online for a short textual description of what they would do in a hypothetical conflict situation.
\item annotation: crowd sourced workers were prompted online to label collected responses regarding the type of conflict strategy chosen.
\end{enumerate}

\section{Data Description and Cleaning}

Data concerning responders had a variety of features and can be divided in two categories: demographic and personality. Demographic data includes: sex, as male or female (no other tag was available); age, in 6 ranges (6-12, 13-18, 19-25, 26-40, 41-65, 66 or older); education, in categories (Never graduated high school, between high school and graduate\footnote{The careful reader will notice that this category is too broad. In the original data two education values had ambiguous labeling. Unable to recover the specific labels I was forced to map the instances to a broad common category.}, Graduated college, A graduate degree other than an M.D. or Ph.D., An M.D. or Ph.D.); number of cell phone numbers in ranges (No cell phone, 1-9, 10-39, 40-99, 100-219, 220-259, 260 or more); number range of text messages received per week (None, 1-9, 10-39, 40-99, 100-219, 220 or more); number of social network friends in ranges (None, 1-9, 10-39, 40-99, 100-219, 220 or more); number of physical exercise hours per week (None, 1-2, 3-5, 6-8, 9 or more); number of video game hours played per day (None, 1, 2, 3, 4, 5 or more); number of tv hours watched per day (None, 1, 2, 3, 4, 5 or more); and textual description of closest town.
% TODO: Create table with comments

Personality was accessed using a short version of the Big Five Inventory \cite{rammstedt07}. Participants had to answer 11 five point likert scale questions regarding their agreement with a statement of self-description (e.g. I see myself as someone who has few artistic interests). Each question answer is supposed to positively or negatively correlate with one dimension of Big Five (openness, conscientiousness, extraversion, agreeableness, and neuroticism) according to the instrument authors. I calculated a 1 to 5 (low to high) value for each dimension using the following procedure: for positive correlations I considered the actual value in the likert scale, and for negative ones 6 minus the actual value; for each dimension I averaged these scores obtaining a single value.

According to the data collection method, the core scenario attributes of original scenarios should have been maintained in the hypothetical ones \cite{swanson12}. When the original scenarios were gathered crowd workers were asked to input the type of relationship between the people in conflict as a categorical field: Stranger, Acquaintance, Romantically Interested, Friend, Romantically Involved, Close Friend, Spouse. As there are implicit relations between the labels, I created two additional numeric fields: a numeric relationship and a numeric involved. With numeric relationship I tried to encode the importance of the relationship with the following mapping: 1 - stranger; 2 - acquaintance; 3 - romantically interested OR friend; 4 - romantically involved OR close friend; 5 - spouse. With involved I tried to encode if the two main elements in the relationship were potentially involved with the following mapping: 2 - romantically involved OR romantically involved OR spouse; 1 otherwise.

Moreover, for the analysis done so far I considered two of the annotation dimensions on the responses: active and aggressive. Responses were annotated as being as Passive or Active, and orthogonally as Aggressive or non Aggressive according to the following definitions \cite{swanson12}:
\begin{itemize}
\item ``\textit{Active} expresses whether the individual's action is an active step or is directly acknowledging the conflict.''
\item ``\textit{Aggressive} specifies whether the response was hostile towards the other party, for example, shouting or intimidation.''
\end{itemize}

Consider the following example scenario: ``Your next door neighbor recently purchased a motocross bike. He has been testing it out on his front lawn, which has completely torn up the grass and it is extremely loud.''. A passive response might be ``Ignore it as long as he isn't on my lawn'' and an active response would be ``I'd nicely ask him to please try to limit how often he does that since it's so loud''.

There are two annotation data sets: \textit{user study}, with more annotations on a smaller number of scenarios and responses; \textit{corpus}, with more scenarios and responses but less annotations per response. There is overlap between the response instances labeled in both. Since I mostly used \textit{corpus} on the classification task and analysis, I will use annotations to refer to the corpus data set unless stated otherwise. For each dimension in corpus and each response we typically have 7 annotations. We consider the label to be the majority vote between annotations.

I group up variables by data type. Ordinal features, are those for which order between positions is known but not the relative differences between positions \cite{field03}[pp. 7--6]. Ranges and self-reported likert scale are good examples of ordinal data. Consequently, we consider the following features to be ordinal, and will refer to them simply as \textit{ordinal features}: age, education, number of mobile numbers range, number of mobile numbers range, number of text messages, number of social network friends, number of physical hours, number of video game hours, extraversion, agreeableness, conscientiousness, neuroticism, openness, relationship. The \textit{nominal features} (unordered categories) will be sex and involved. Lastly, since scenario description and city are both free form text, we will refer to them as \textit{text features}.

For the reported analysis I performed an inner join merge of demographic, personality, scenario, response and annotation data. Given that to do predictions we would need as rich data as possible I decided to discard instances for which we did not have one of the mentioned subsets of data. Additionally, for some subsets of data I had to merge several file batches with slightly different table schemas. The code and data is publicly available at \href{https://bitbucket.org/pfontain/conflict-data-cleaning}{https://bitbucket.org/pfontain/conflict-data-cleaning}.
% TODO future work consider certainty value
% TODO basic text analysis

The following descriptive statistics refer to the merged data. There were $164$ different scenarios, a total of $1017$ responses (a ratio of $6.2$ responses per scenario), $90$ responders. Regarding the responses responders: $37\%$ male responses, all having at least higher school education (details in Figure~\ref{fig:corpuseducation}), $99\%$ having ages between $19$ and $65$ (details in Figure~\ref{fig:corpus_age}), $83\%$ were labeled as Active, and $16\%$ were labeled as Aggressive.

% TODO: Improve picture
\begin{figure}
        \centering
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{figs/education.png}
                \caption{education level}
                \label{fig:corpuseducation}
        \end{subfigure}%
        ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
          %(or a blank line to force the subfigure onto a new line)
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{figs/age_group.png}
                \caption{age}
                \label{fig:corpus_age}
        \end{subfigure}
        \caption{Responders' demographics}\label{fig:demographic}
\end{figure}

\section{Problem Knowledge}

Regarding prediction I will further focus on Active/Passive annotations. When the conflict corpus was gathered, the implicit hypothesis was that one or more features (scenario, demographic or personality) would determine conflict response. Additionally, there has been empirical evidence supporting the hypothesis that extroverts tend to be more integrative and assertive in their responses \cite{kilmann75}. Consequently, one can hypothesize that extroverts will have responses that will be perceived as more active. We should expect differences of feature distribution between response groups (Active vs Passive). In order to detect these relevant relations I performed a statistical analysis on a subset of the \textit{user study} whose responses were not included in the test set (described in Results). We will call it the \textit{dev train} set.

I initially calculated Spearman's rank correlation coefficients between features and the active labels converted to binary values (0 or 1). However, I realized that performing a Wilcoxon Mann-Whitney rank sum test for each ordinal feature, grouping responses by annotation label was more adequate and would provide better information. The test results (p-value and effect size), with medians for each feature/label on the \textit{dev train} are presented in Table~\ref{tab:wilcoxon}. The features for which there is a significant difference considering $p<0.05$ are the following: age (higher in Passive) \footnote{In the class project presentation this correlation was incorrectly described.}; number mobile number (higher in Active); number text messages (higher in Active); number physical hours (higher in Active); extraversion (higher in Active); neuroticism (higher in Active). Note that since we had a more restrictive hypothesis in extraversion we could actually consider the one-tailed p-value ($0.0230$).

% latex table generated in R 3.0.3 by xtable 1.7-3 package
% Sun Jun  8 17:50:01 2014
\begin{table}
\begin{tabular}{rrrrr}
  \hline
 & Mdn Act & Mdn Pas & p & r \\ 
  \hline
age\_group & 4.00 & 5.00 & 0.0303 & -0.2211 \\ 
  education & 2.00 & 3.00 & 0.1931 & -0.1328 \\ 
  n\_mobile\_numbers & 4.00 & 3.00 & 0.0304 & 0.2209 \\ 
  n\_text\_messages & 4.00 & 3.00 & 0.0378 & 0.2120 \\ 
  n\_friends\_sn & 4.00 & 4.00 & 0.0773 & 0.1803 \\ 
  n\_hours\_pa & 3.00 & 3.00 & 0.0163 & 0.2451 \\ 
  n\_hours\_vg\_day & 2.50 & 2.00 & 0.1364 & 0.1520 \\ 
  n\_hours\_tv\_week & 4.00 & 3.00 & 0.0810 & 0.1781 \\ 
  bfi\_extraversion & 3.50 & 2.50 & 0.0460 & 0.2037 \\ 
  bfi\_agreeableness & 4.00 & 3.33 & 0.0650 & 0.1883 \\ 
  bfi\_conscientiousness & 4.50 & 4.50 & 0.5259 & -0.0647 \\ 
  bfi\_neuroticism & 2.00 & 3.50 & 0.0134 & -0.2524 \\ 
  bfi\_openness & 4.50 & 4.50 & 0.2508 & 0.1172 \\ 
  friendship\_num & 4.00 & 3.00 & 0.2077 & 0.1286 \\ 
   \hline
\end{tabular}
  \caption{Mann-Whitney rank sum test results. \textit{Mdn Act} - median value for active response group, \textit{Mdn Pas} - median value for passive response group, \textit{p} p-value, \textit{r} effect size.}
  \label{tab:wilcoxon}
\end{table}

\section{Model}

In order to make predictions on the Active label of responses I resorted to Probabilistic Soft Logic (PSL) \cite{kimmig12}\footnote{Since PSL was described at length in cmps290C I will abstain from bloating this report with its description.}. I encoded the following predicates:
\begin{itemize}
\item active(X,Y): Response with id X has active label Y as a partial function (\textit{Active} or \textit{Passive}).
\item responded(X,Y): Response with id X was written by responder with id Y.
\item responseScenario(X,Y): Response with id X regarded scenario with id Y.
\item extraversion(X,Y): Responder with id X has extraversion value Y.
\item neuroticism(X,Y): Responder with id X has neuroticism value Y.
\item physical(X,Y): Responder with id X has physical activity value Y.
\item isHigh(X): personality value X is above a threshold (bfi-high-threshold).
\item isLow(X): personality value X is below a threshold (bfi-low-threshold).
\item isHighP(X): physical activity value X is above a threshold (pa-high-threshold).
\item isLowP(X): physical activity X is below a threshold (pa-low-threshold).
\item olderAdult(X): age X is above threshold (age-high-threshold).
\item youngerAdult(X): age X is below threshold (age-low-threshold).
\item sameExtraversion(X,Y): difference between extraversion X and Y is smaller than threshold (extraversion-similar-threshold).
\end{itemize}

I made two collective knowledge assumptions: a scenario should tend to be answered in the same way by different responders; the same responder will tend to respond in the same way to different scenarios. Given these assumptions and the problem knowledge I encoded the following soft inference rules:
\begin{enumerate}
\item \begin{verbatim} (responded(A,X) & responded(B,X) & active(A,C)) >> active(B,C) \end{verbatim} If a worker responds one way in a scenario it is likely the response will be similar in another scenario.

\item \begin{verbatim} ( responded(A,B) & extraversion(B,C) & isHigh(C)) >>
active(A,activeStringAttribute)
\end{verbatim} Extroverts tend to have active responses.

\item \begin{verbatim} ( responded(A,B) & extraversion(B,C) & isLow(C)) >>
active(A,passiveStringAttribute)
\end{verbatim} Introverts tend to have passive responses.

\item
\begin{verbatim} ( responseScenario(A,D) & responseScenario(B,D) &
responded(A,X) & responded(B,Y) &
extraversion(X,F) & extraversion(Y,G) &
sameExtraversion(F,G) & active(A,C)) >>
active(B,C)
\end{verbatim} If two responders have similar extraversion values they will tend to have similar responses to the same scenario.

\item \begin{verbatim} ( responded(A,B) & neuroticism(B,C) & isHigh(C)) >>
active(A,passiveStringAttribute)
\end{verbatim} Responders with higher neuroticism values tend to have passive responses.

\item \begin{verbatim} ( responded(A,B) & neuroticism(B,C) & isLow(C)) >>
active(A,activeStringAttribute)
\end{verbatim} Responders with lower neuroticism values tend to have active responses.

\item \label{item:physicalH} \begin{verbatim} ( responded(A,B) & physical(B,C) & isHighP(C)) >>
active(A,activeStringAttribute)
\end{verbatim} Responders who do more physical activity tend to have active responses.

\item \label{item:physicalL} \begin{verbatim} ( responded(A,B) & physical(B,C) & isLowP(C)) >>
active(A,passiveStringAttribute)
\end{verbatim} Responders who do less physical activity tend to have passive responses.

\item \label{item:ageH} \begin{verbatim} ( responded(A,B) & age(B,C) & youngerAdult(C)) >>
active(A,activeStringAttribute)
\end{verbatim} Younger adult responders tend to have active responses.

\item \label{item:ageL} \begin{verbatim} ( responded(A,B) & age(B,C) & olderAdult(C)) >>
active(A,passiveStringAttribute)
\end{verbatim} Older adult responders tend to have passive responses.

\item \begin{verbatim} ( responseScenario(A,X) & responseScenario(B,X) & active(A,C)) >>
active(B,C)
\end{verbatim} If a worker responds one way in a scenario it is likely that another worker will respond in the same way.
\end{enumerate}

Note that I did not encode rules regarding the number of mobile numbers or text messages. This had to do with the fact that this encoding, although supported by the rank statistical analysis, was initially inspired by another set of correlations in which these features did not appear predictive. The weights for the presented rules were learned using lazy most probable explanation inference, with initial values set as 1.

Looking back at the problem knowledge, we note that there is less evidence for age and physical activity distribution differences across label groups. Consequently, it is reasonable to ask if the rules regarding these two features should be kept. I performed an ablation study analyzing the effect of removing one or more of these sets of rules, training on the train set and and testing on the dev train set. I had four conditions: all rules; no physical rules (\ref{item:physicalH} and \ref{item:physicalL}); no age rules (\ref{item:ageH} and \ref{item:ageL}); no age or physical rules. Using as performance measure the area under the curve (AUC) of the receiver operating characteristic (ROC) with the probability ranking as threshold I had: $0.789$ with all rules, $0.779$ for no physical rules, $0.755$ for no age rules, and $0.689$ for neither age nor physical rules. Since including all rules presented the highest performance, I did not remove any.

Regarding the rule thresholds, I used the following values: bfi-high-threshold = $3.5$, bfi-low-threshold = $2.5$, pa-high-threshold = $3.5$, pa-low-threshold = $2.5$, age-high-threshold = $5$, age-low-threshold = $3$, extraversion-similar-threshold = $0.5$. These thresholds should probably be learned, however their ad-hoc choice was motivated: personality values ranges from 1 to 5, making 3 the middle value in the absolute scale; age range median in training data was 4; extraversion values were real numbers but they tended to cluster in approximate integer values (e.g. 3); the physical activity range median in training data was 3.

\section{Results}

In order to test the model I split the corpus responses randomly in train and test set ($25\%$). Moreover, I used a naive bayes classifier as baseline. The baseline was trained with ordinal and nominal features, fitting gaussians to ordinal values and using frequencies for the nominal ones. Cells with 0 probability were replaced by $0.001$. The AUC-ROC performance was the following: baseline $0.580$, PSL model $0.553$. Sadly, it appears the PSL model performs worse in the test set then the baseline. The ROC curve is presented in Figure~\ref{fig:ROC}. Over most  thresholds of probability ranking, the baseline performs better.

\begin{figure}
        \includegraphics[width=0.7\textwidth]{figs/ROC.pdf}
        \caption{ROC curve for naive bayes baseline (dashed line) and PSL model (full line).}
        \label{fig:ROC}
\end{figure}

\section{Discussion}

In summary, I developed an active label conflict response predictor in PSL. The rules encoded were inspired in related work and statistical analysis of a subset of the training data for which we have higher confidence on the labels. However, when compared to a naive bayes baseline predictor the ROC-AUC performance was lower.

I will discuss possible reasons for these results. Regarding the rule threshold selection, their ad-hoc choice might be harming performance. An alternative would be to use expectation maximization considering these thresholds as latent features. Another problem is that I provided less information to the PSL model than to the naive bayes classifier. Including information such as number of text messages might improve results. Concerning feature processing, converting city to a geographic attribute and using geographic proximity as similarity could also be used in PSL's advantage. Additionally, the random data split may hamper the PSL model performance due to its collective nature. A snowball sampling method might be more adequate. 

Nonetheless, the domain and data themselves might present challenges to a probabilistic soft logic approach. Specifically, with not so strong problem knowledge, as we have in this case, encoding rules that lead to overfitting might be more likely. Furthermore, both models could take advantage of textual features of the response, such as word sentiment polarity.

In addition to exploring textual features, the data merits further grouping statistical analysis using the nominal features sex and involved. More non collective classifiers should be tried such as logistic regression and support vector machines. The relation between a person's weekly physical activity and type of conflict strategy chosen should be further tested in a more targeted study.

The current version of the data and cleaning, together with instructions on how to run it, is available with tag \textit{v1.2.3} at:

\href{https://bitbucket.org/pfontain/conflict-data-cleaning}{https://bitbucket.org/pfontain/conflict-data-cleaning}

The PSL model code, together with instructions and this report, is available with tag \textit{v1.2} at:

\href{https://bitbucket.org/pfontain/psl-conflict-strategy-classification}{https://bitbucket.org/pfontain/psl-conflict-strategy-classification}

% Although I did not encounter any significant performance issues, one could consider the following improvements to the psl rules: ... order rules for performance ... empty fields instead of separate predicates
% use training set for correlations
% not strong knowledge in domain

\bibliographystyle{apalike} 
\bibliography{references}

\end{document}

